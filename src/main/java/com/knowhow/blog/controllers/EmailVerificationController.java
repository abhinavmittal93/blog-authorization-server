package com.knowhow.blog.controllers;

import com.knowhow.blog.entities.VerificationKeyModel;
import com.knowhow.blog.payload.ApiResponse;
import com.knowhow.blog.service.UserService;
import com.knowhow.blog.service.VerificationKeyService;
import com.knowhow.blog.utility.ResponseCodeConstants;
import com.knowhow.server.entities.UserModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/verify")
public class EmailVerificationController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private VerificationKeyService verificationKeyService;

	@Autowired
	private UserService userService;

	@GetMapping("/email")
	public ResponseEntity<String> verifyUserByVerificationKey(@RequestParam(name = "verificationKey") String verificationKey) {
		logger.info("verifyUserByVerificationKey(): begins, verificationKey:{}", verificationKey);
		VerificationKeyModel verificationKeyModel = verificationKeyService.getVerificationKeyDetailsByKeyWithUpdatedExpiredStatus(verificationKey);

		if (verificationKeyModel == null) {
			return ResponseEntity.ok().body(ResponseCodeConstants.NOT_FOUND);
		}

		if (verificationKeyModel.isUsed()) {
			return ResponseEntity.ok().body(ResponseCodeConstants.ALREADY_USED_KEY);
		}

		if (verificationKeyModel.isExpired()) {
			return ResponseEntity.ok().body(ResponseCodeConstants.EXPIRED_VERIFICATION_KEY);
		}

		try {
			boolean verified = verificationKeyService.decryptVFKeyAndVerify(verificationKey);
			if(verified) {
				return ResponseEntity.ok().body(ResponseCodeConstants.SUCCESS);
			}
		} catch (Exception e) {
			return ResponseEntity.ok().body(ResponseCodeConstants.ERROR);
		}
		return ResponseEntity.ok().body(ResponseCodeConstants.INVALID_VERIFICATION_KEY);
	}

	@PostMapping(name = "/resend/link")
	public ResponseEntity<ApiResponse> resendEmailVerificationLink(@RequestParam(name = "email") String email) {
		logger.info("resendEmailVerificationLink(): begins, email:{}", email);
		ApiResponse apiResponse = new ApiResponse(false, "Something went wrong. Please try again!", ResponseCodeConstants.EXCEPTION_OCCURED, null);
		if(StringUtils.isBlank(email)) {
			return ResponseEntity.badRequest().body(apiResponse);
		} else if(!userService.existsByEmail(email)) {
			return ResponseEntity.badRequest().body(apiResponse);
		} else {
			Optional<UserModel> optionalUserModel = userService.findByEmail(email);
			if(optionalUserModel.isPresent()) {
				UserModel userModel = optionalUserModel.get();
				if(userModel.isVerified()) {
					apiResponse = new ApiResponse(false, "Account with this email is already verified."
							, ResponseCodeConstants.EMAIL_ALREADY_VERIFIED, email);
				} else {
					verificationKeyService.resendVerificationLink(email);
					apiResponse = new ApiResponse(false, "An email with verification link has been sent at <b>" + email + "</b>."
							, ResponseCodeConstants.SUCCESS, email);
				}
			}
		}
		return ResponseEntity.badRequest().body(apiResponse);
	}
}