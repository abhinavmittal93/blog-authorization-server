package com.knowhow.blog.utility;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertyConstants {

	@Value("${ui.app.url}")
	private String uiAppURL;

	public String getUiAppURL() {
		return uiAppURL;
	}

	public void setUiAppURL(String uiAppURL) {
		this.uiAppURL = uiAppURL;
	}
}
