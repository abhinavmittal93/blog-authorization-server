package com.knowhow.blog.utility;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class SendgridEmail {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Value("${sendgrid.api.key}")
	private String sendgridApiKey;

	private static final String FROM_EMAIL = "abhinavmittal93@gmail.com";

	private static final String VERFICATION_TEMPLATE_ID = "d-f262b65542f540cc9783e37caf810c01";

	private static final String VERIFICATION_URL_REPLACE_CONSTANT = "{{verification_url}}";

	public void sendEmail(String to, String subject, String body) throws IOException {
		Email from = new Email(FROM_EMAIL);
		Content content = new Content("text/html", body);
		Mail mail = new Mail(from, subject, new Email(to), content);
		SendGrid sg = new SendGrid(sendgridApiKey);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sg.api(request);
		} catch (Exception ex) {
			logger.error("Exception while sending email", ex.getMessage(), ex);
			throw ex;
		}
	}

	@Async
	public void sendVerificationEmail(String to, String url) throws IOException {
		JsonObject jsonObject = getVerificationTemplate();
		String body = jsonObject.get("html_content").getAsString();
		body = body.replace(VERIFICATION_URL_REPLACE_CONSTANT, url);
		String subject = jsonObject.get("subject").getAsString();
		sendEmail(to, subject, body);
	}

	private JsonObject getVerificationTemplate() throws IOException {
		SendGrid sg = new SendGrid(sendgridApiKey);
		Request request = new Request();
		JsonObject jsonObject = null;
		try {
			request.setMethod(Method.GET);
			request.setEndpoint("templates/" + VERFICATION_TEMPLATE_ID + "/versions/aa650051-98cc-472b-ae8b-99b1c1d8a749");
			Response response = sg.api(request);
			System.out.println(response.getStatusCode());
			System.out.println(response.getBody());
			System.out.println(response.getHeaders());
			jsonObject = new JsonParser().parse(response.getBody()).getAsJsonObject();
		} catch (Exception ex) {
			logger.error("Exception while getting verification email template", ex.getMessage(), ex);
			throw ex;
		}
		return jsonObject;
	}
}
