package com.knowhow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogOauthServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogOauthServerApplication.class, args);
	}

}
