package com.knowhow.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.security.Principal;

@RestController
@RequestMapping("/auth")
public class AuthController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private DataSource dataSource;

	@GetMapping("/principal")
	public Principal user(Principal principal) {
		return principal;
	}

	@GetMapping("/logout")
	public ResponseEntity<String> revoke(HttpServletRequest request) {
		JdbcTokenStore jdbcTokenStore = new JdbcTokenStore(dataSource);
		try {
			String authorization = request.getHeader("Authorization");
			if (authorization != null && authorization.contains("Bearer")) {
				String tokenValue = authorization.replace("Bearer", "").trim();

				OAuth2AccessToken accessToken = jdbcTokenStore.readAccessToken(tokenValue);
				jdbcTokenStore.removeAccessToken(accessToken);

				OAuth2RefreshToken refreshToken = accessToken.getRefreshToken();
				jdbcTokenStore.removeRefreshToken(refreshToken);
			}
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("Invalid access token");
		}

		return ResponseEntity.ok().body("Success");
	}



}
