package com.knowhow.server.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.knowhow.server.entities.RoleModel;
import com.knowhow.server.utility.RoleName;

@Repository
public interface RoleRepository extends JpaRepository<RoleModel, Long> {

	Optional<RoleModel> findByName(RoleName roleName);

}
