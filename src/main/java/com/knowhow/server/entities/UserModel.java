package com.knowhow.server.entities;

import com.knowhow.blog.audit.DateAuditModel;
import org.hibernate.annotations.NaturalId;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users", uniqueConstraints = {@UniqueConstraint(columnNames = {"username"}),
		@UniqueConstraint(columnNames = {"email"})})
public class UserModel extends DateAuditModel {

	public static int COLUMN_LENGTH_USERNAME = 25;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NaturalId
	@Column(length = 255, nullable = false)
	private String email;

	@NaturalId
	@Column(length = 25, nullable = false)
	private String username;

	@Column(length = 1000, nullable = false)
	private String password;

	@Column(name = "first_name", length = 255, nullable = false)
	private String firstName;

	@Column(name = "middle_name", length = 255, nullable = true)
	private String middleName;

	@Column(name = "last_name", length = 255, nullable = false)
	private String lastName;

	@Column(name = "active", insertable = false, nullable = false)
	private boolean isActive;

	@Column(name = "verfied", insertable = false, nullable = false)
	private boolean isVerified;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<RoleModel> roles = new HashSet<>();

	public UserModel() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		email = email.trim().toLowerCase();
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		username = username.trim().toLowerCase();
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		password = new BCryptPasswordEncoder().encode(password);
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean active) {
		isActive = active;
	}

	public boolean isVerified() {
		return isVerified;
	}

	public void setVerified(boolean verified) {
		isVerified = verified;
	}

	public Set<RoleModel> getRoles() {
		return roles;
	}

	public void setRoles(Set<RoleModel> roles) {
		this.roles = roles;
	}
}
