package com.knowhow.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@EnableAsync
@EnableResourceServer
@Configuration
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	private static final String RESOURCE_ID = "resource_id";

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.resourceId(RESOURCE_ID).stateless(false);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
//		http.anonymous().disable()
//				.authorizeRequests()
//				.antMatchers("/auth/**", "/api/user/**", "/api/verify/**").permitAll()
//				.and()
//				.exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());


				http
				.anonymous().and()
				.authorizeRequests()
						.antMatchers("/auth/**", "/api/user/**", "/api/verify/**","/api/signup", "/api/signup/**").permitAll()
						.and()
						.exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
	}

}
